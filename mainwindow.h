#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlDatabase>

class QHBoxLayout;
class QSqlTableModel;
class QTableView;
class GanttDiagram;

class MainWindow : public QMainWindow
{
	Q_OBJECT
public:
	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();

public slots:
	void onSettingsPressed();
	void onDiagramPressed();

	void addProject();
	void deleteProject();

	void addJob();
	void deleteJob();

	void addRisk();
	void setRiskImpact();
	void deleteRisk();

	void onProjectSelectionChanged();
	void onJobSelectionChanged();

private:
	bool reconnect();
	void updateTable(QSqlTableModel* table);

	QString m_host;
	QString m_port;
	QString m_user;
	QString m_password;
	QString m_database;

	QSqlDatabase m_db;

    QSqlTableModel* m_projectTable = nullptr;
    QSqlTableModel* m_jobTable = nullptr;
    QSqlTableModel* m_riskTable = nullptr;

    QTableView* m_projectView = nullptr;
    QTableView* m_jobView = nullptr;
    QTableView* m_riskView = nullptr;

    GanttDiagram* m_diagram = nullptr;

signals:

};

#endif // MAINWINDOW_H
