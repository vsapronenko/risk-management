#ifndef IMPACTEDITOR_H
#define IMPACTEDITOR_H
#include <QWizard>

class QSpinBox;
class QFormLayout;
class QTextEdit;

class ImpactEditor : public QWizard
{
public:
	ImpactEditor(int riskId);

private:
	void changeExpertCount(int);
	void updateLog();
	void saveToDatabase();
	void calculate(bool saveToDatabase);

	QSpinBox* m_expertCount;
	QFormLayout* m_impactForm[4];
	QTextEdit* m_log[4];
	int m_riskId;
};

#endif // IMPACTEDITOR_H
