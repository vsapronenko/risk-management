#include "impacteditor.h"
#include <QLabel>
#include <QFormLayout>
#include <QSpinBox>
#include <QTextEdit>
#include <QAbstractButton>
#include <cmath>
#include <QSqlQuery>

const int MAX_EXPERT_COUNT = 10;
const int DEFAULT_EXPERT_COUNT = 2;

const QString impactName[4] = {
	QStringLiteral("Влияние на бюджет"),
	QStringLiteral("Влияние на сроки"),
	QStringLiteral("Влияние на качество"),
	QStringLiteral("Влияние на содержание"),
};

const QString fieldName[4] = {
	QStringLiteral("impact_budget"),
	QStringLiteral("impact_end_date"),
	QStringLiteral("impact_quality"),
	QStringLiteral("impact_content"),
};


ImpactEditor::ImpactEditor(int riskId) : m_riskId(riskId)
{
	setWindowTitle("Оценка влияния риска");
	QWizardPage* page = new QWizardPage(this);
	QFormLayout* form = new QFormLayout;
	m_expertCount = new QSpinBox(this);
	m_expertCount->setValue(DEFAULT_EXPERT_COUNT);
	m_expertCount->setRange(1, MAX_EXPERT_COUNT);
	form->addRow("Количество экспертов", m_expertCount);
	connect(m_expertCount, QOverload<int>::of(&QSpinBox::valueChanged), this, &ImpactEditor::changeExpertCount);
	page->setLayout(form);
	addPage(page);

	for (int i = 0; i < 4; i++) {
		page = new QWizardPage(this);
		page->setTitle("asdfa");
		form = new QFormLayout;
		for (int j = 0; j < MAX_EXPERT_COUNT; j++) {
			QSpinBox* field = new QSpinBox(this);
			connect(field, QOverload<int>::of(&QSpinBox::valueChanged), this, &ImpactEditor::updateLog);
			form->addRow("Експерт " + QString::number(j + 1), field);
		}
		m_log[i] = new QTextEdit(this);
		form->addWidget(m_log[i]);
		m_impactForm[i] = form;
		page->setLayout(form);
		addPage(page);
	}

	changeExpertCount(DEFAULT_EXPERT_COUNT);
	connect(button(QWizard::FinishButton), &QAbstractButton::clicked, this, &ImpactEditor::saveToDatabase);
}

void ImpactEditor::changeExpertCount(int expertCount)
{
	for (int i = 0; i < 4; i++) {
		m_log[i]->setVisible(expertCount > 1);

		QFormLayout* form = m_impactForm[i];
		for (int j = 0; j < MAX_EXPERT_COUNT; j++) {
			bool visible = j < expertCount;
			form->itemAt(j, QFormLayout::LabelRole)->widget()->setVisible(visible);
			form->itemAt(j, QFormLayout::FieldRole)->widget()->setVisible(visible);
		}
	}
}

void ImpactEditor::updateLog()
{
	calculate(false);
}

void ImpactEditor::saveToDatabase()
{
	calculate(true);
}

void ImpactEditor::calculate(bool saveToDatabase)
{
	int expertCount = m_expertCount->value();
	for (int i = 0; i < 4; i++) {	// TODO: add log to m_log
		QFormLayout* form = m_impactForm[i];
		double sum = 0;
		QList<int> impacts;
		QStringList strList;
		QString log;
		for (int j = 0; j < expertCount; j++) {
			QSpinBox* impactEditor = dynamic_cast<QSpinBox*>(form->itemAt(j, QFormLayout::FieldRole)->widget());
			int impact = impactEditor->value();
			sum += impact;
			strList << QString::number(impact);
			impacts << impact;
		}
		double averageImpact = sum / expertCount;
		log += "Средняя оценка: (" + strList.join(" + ") + ") / " + QString::number(expertCount) + " = " + QString::number(averageImpact) + "\n";
		strList.clear();
		double deviation = 0;
		for (int impact : impacts) {
			double diff = averageImpact - impact;
			deviation += diff * diff;
			strList << QString::number(diff) + "²";
		}
		deviation = sqrt(deviation / expertCount);
		log += "Среднеквадратическое отклонение: √((" + strList.join(" + ") + ") / " + QString::number(expertCount) + ") = " + QString::number(deviation) + "\n";
		log += "Оставляем только в диапазоне " + QString::number(averageImpact) + " ± " + QString::number(deviation) +
				" = {" + QString::number(averageImpact - deviation) + "; " + QString::number(averageImpact + deviation) + "}\n";
		strList.clear();
		QList<int> filteredImpacts;
		double filteredAverage = 0;
		double filteredCount = 0;
		for (int impact : impacts) {
			if (averageImpact - deviation <= impact && impact <= averageImpact + deviation) {
				filteredAverage += impact;
				strList << QString::number(impact);
				filteredCount++;
			}
		}
		filteredAverage /= filteredCount;
		int result = std::round(filteredAverage);
		log += "Из оставшихся средняя оценка: (" + strList.join(" + ") + ") / " + QString::number(filteredCount) +
				" = " + QString::number(filteredAverage) +
				" ≈ " + QString::number(result) + "\n";
		if (saveToDatabase) {
			QSqlQuery riskUpdateQuery("UPDATE risks "
									  "SET " + fieldName[i] + " = " + QString::number(result) + " " +
									  "WHERE id = " + QString::number(m_riskId) + ";");
		} else {
			m_log[i]->setText(log);
		}
	}
}

