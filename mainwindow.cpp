#include "mainwindow.h"
#include "settings.h"
#include <QHBoxLayout>
#include <QMessageBox>
#include <QAction>
#include <QMenuBar>
#include <QFormLayout>
#include <QLineEdit>
#include <QDialogButtonBox>
#include <QSqlTableModel>
//#include <QSqlRelationalTableModel>
#include <QTableView>
#include <QSqlError>
#include <QSettings>
#include "ganttdiagram.h"
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QSplitter>
#include <QSqlQuery>
#include <QInputDialog>
#include <QHeaderView>
#include <QSqlRecord>
#include "impacteditor.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
	QSettings settings;
	m_host = settings.value("host", dbHost).toString();
	m_port = settings.value("port", dbPort).toString();
	m_user = settings.value("user", dbUser).toString();
	m_password = settings.value("password", dbPass).toString();
	m_database = settings.value("database", dbName).toString();

	QAction* action = new QAction("Настройки");	// Создается меню "Настройки"
	connect(action, &QAction::triggered, this, &MainWindow::onSettingsPressed);	// На пункт меню "подключается" обработчик onSettingsPressed
	menuBar()->addAction(action);	// Пункт меню добавляется на панель

	action = new QAction("Диаграма");	// Создается меню "Диаграма"
	connect(action, &QAction::triggered, this, &MainWindow::onDiagramPressed);	// На пункт меню "подключается" обработчик onSettingsPressed
	menuBar()->addAction(action);	// Пункт меню добавляется на панель

	//m_db = QSqlDatabase::addDatabase("QMYSQL");	// Инициализируется плагин для работы с базой MySql
	m_db = QSqlDatabase::addDatabase("QPSQL");	// Инициализируется плагин для работы с базой PostgreSQL

	if (!reconnect()) {
		return;
	}

	// Добавляем в главное окно таблицы с данными
	QSplitter* container = new QSplitter(this);
	setCentralWidget(container);

	// Таблица с проектами
	m_projectTable = new QSqlTableModel(this, m_db);
	m_projectTable->setTable("projects");
	m_projectTable->setEditStrategy(QSqlTableModel::OnFieldChange);
	m_projectTable->setSort(0, Qt::AscendingOrder);

	updateTable(m_projectTable);

	m_projectTable->setHeaderData(1, Qt::Horizontal, tr("Наименование"));
	m_projectTable->setHeaderData(2, Qt::Horizontal, tr("Описание"));
	//m_projectTable->setHeaderData(3, Qt::Horizontal, tr("Периметр"));
	//m_projectTable->setHeaderData(4, Qt::Horizontal, tr("Руководитель"));
	m_projectTable->setHeaderData(3, Qt::Horizontal, tr("Бюджет"));
	m_projectTable->setHeaderData(4, Qt::Horizontal, tr("Начало"));
	m_projectTable->setHeaderData(5, Qt::Horizontal, tr("Конец"));
	m_projectTable->setHeaderData(6, Qt::Horizontal, tr("Просрочен"));
	m_projectTable->setHeaderData(7, Qt::Horizontal, tr("Длительность"));

	m_projectView = new QTableView(this);
	m_projectView->setModel(m_projectTable);
	m_projectView->hideColumn(0);	// ID
	m_projectView->resizeColumnsToContents();

	m_projectView->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);
	m_projectView->horizontalHeader()->setHighlightSections(false);

	connect(m_projectView->selectionModel(), &QItemSelectionModel::selectionChanged, this, &MainWindow::onProjectSelectionChanged);

	QVBoxLayout* column = new QVBoxLayout;
	QWidget* columnWidget = new QWidget(this);
	columnWidget->setLayout(column);
	column->addWidget(new QLabel("Проекты", this));
	column->addWidget(m_projectView);

	QHBoxLayout* buttons = new QHBoxLayout();
	QPushButton* addButton = new QPushButton("Добавить", this);
	connect(addButton, &QPushButton::pressed, this, &MainWindow::addProject);
	buttons->addWidget(addButton);
	QPushButton* deleteButton = new QPushButton("Удалить", this);
	connect(deleteButton, &QPushButton::pressed, this, &MainWindow::deleteProject);
	buttons->addWidget(deleteButton);
	buttons->addStretch();
	column->addLayout(buttons);

	container->addWidget(columnWidget);

	// Таблица с работами
	m_jobTable = new QSqlTableModel(this, m_db);
	m_jobTable->setTable("works");
	m_jobTable->setEditStrategy(QSqlTableModel::OnFieldChange);
	m_jobTable->setSort(0, Qt::AscendingOrder);

	updateTable(m_jobTable);

	m_jobTable->setHeaderData(1, Qt::Horizontal, tr("Наименование"));
	m_jobTable->setHeaderData(3, Qt::Horizontal, tr("Начало"));
	m_jobTable->setHeaderData(4, Qt::Horizontal, tr("Конец"));
	m_jobTable->setHeaderData(5, Qt::Horizontal, tr("Просрочен"));
	m_jobTable->setHeaderData(6, Qt::Horizontal, tr("Длительность"));
	m_jobTable->setHeaderData(7, Qt::Horizontal, tr("Стоимость"));
	m_jobTable->setHeaderData(8, Qt::Horizontal, tr("Исполнитель"));
	m_jobTable->setHeaderData(9, Qt::Horizontal, tr("Совокупная важность рисков работы"));

	m_jobView = new QTableView(this);
	m_jobView->setModel(m_jobTable);
	m_jobView->hideColumn(0);	// ID
	m_jobView->hideColumn(2);	// Project
	m_jobView->resizeColumnsToContents();

	m_jobView->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);
	m_jobView->horizontalHeader()->setHighlightSections(false);

	connect(m_jobView->selectionModel(), &QItemSelectionModel::selectionChanged, this, &MainWindow::onJobSelectionChanged);

	column = new QVBoxLayout;
	columnWidget = new QWidget(this);
	columnWidget->setLayout(column);
	column->addWidget(new QLabel("Работы", this));
	column->addWidget(m_jobView);

	buttons = new QHBoxLayout();
	addButton = new QPushButton("Добавить", this);
	connect(addButton, &QPushButton::pressed, this, &MainWindow::addJob);
	buttons->addWidget(addButton);
	deleteButton = new QPushButton("Удалить", this);
	connect(deleteButton, &QPushButton::pressed, this, &MainWindow::deleteJob);
	buttons->addWidget(deleteButton);
	buttons->addStretch();
	column->addLayout(buttons);

	container->addWidget(columnWidget);

	// Таблица с рисками
	m_riskTable = new QSqlTableModel(this, m_db);
	m_riskTable->setTable("risks");
	m_riskTable->setEditStrategy(QSqlTableModel::OnFieldChange);
	m_riskTable->setSort(0, Qt::AscendingOrder);

	updateTable(m_riskTable);

	m_riskTable->setHeaderData(1, Qt::Horizontal, tr("Наименование"));
	m_riskTable->setHeaderData(3, Qt::Horizontal, tr("Меры для уменьшения"));
	m_riskTable->setHeaderData(4, Qt::Horizontal, tr("Вероятность"));
	m_riskTable->setHeaderData(5, Qt::Horizontal, tr("Влияние на бюджет"));
	m_riskTable->setHeaderData(6, Qt::Horizontal, tr("Вес влияния на бюджет"));
	m_riskTable->setHeaderData(7, Qt::Horizontal, tr("Влияние на сроки"));
	m_riskTable->setHeaderData(8, Qt::Horizontal, tr("Вес влияния на сроки"));
	m_riskTable->setHeaderData(9, Qt::Horizontal, tr("Влияние на качество"));
	m_riskTable->setHeaderData(10, Qt::Horizontal, tr("Вес влияния на качество"));
	m_riskTable->setHeaderData(11, Qt::Horizontal, tr("Влияние на содержание"));
	m_riskTable->setHeaderData(12, Qt::Horizontal, tr("Вес влияния на содержание"));
	m_riskTable->setHeaderData(13, Qt::Horizontal, tr("Важность риска"));
	m_riskTable->setHeaderData(14, Qt::Horizontal, tr("Вес риска"));

	m_riskView = new QTableView(this);
	m_riskView->setModel(m_riskTable);
	m_riskView->hideColumn(0);	// ID
	m_riskView->hideColumn(2);	// Work
	m_riskView->resizeColumnsToContents();

	m_riskView->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);
	m_riskView->horizontalHeader()->setHighlightSections(false);

	column = new QVBoxLayout;
	columnWidget = new QWidget(this);
	columnWidget->setLayout(column);
	column->addWidget(new QLabel("Риски", this));
	column->addWidget(m_riskView);

	buttons = new QHBoxLayout();
	addButton = new QPushButton("Добавить", this);
	connect(addButton, &QPushButton::pressed, this, &MainWindow::addRisk);
	buttons->addWidget(addButton);
	QPushButton* setImpactButton = new QPushButton("Задать коэффициенты влияния", this);
	connect(setImpactButton, &QPushButton::pressed, this, &MainWindow::setRiskImpact);
	buttons->addWidget(setImpactButton);
	deleteButton = new QPushButton("Удалить", this);
	connect(deleteButton, &QPushButton::pressed, this, &MainWindow::deleteRisk);
	buttons->addWidget(deleteButton);
	buttons->addStretch();
	column->addLayout(buttons);

	container->addWidget(columnWidget);

	onProjectSelectionChanged();
}

MainWindow::~MainWindow()
{
	if (m_diagram != nullptr) {
		delete m_diagram;
	}
}

void MainWindow::onSettingsPressed()
{
	QDialog settingsDialog(this, Qt::WindowSystemMenuHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint);
	QFormLayout* fl = new QFormLayout(&settingsDialog);

	QLineEdit* hostEdit = new QLineEdit(&settingsDialog);
	hostEdit->setText(m_host);
	fl->addRow(tr("Сервер"), hostEdit);

	QLineEdit* portEdit = new QLineEdit(&settingsDialog);
	portEdit->setText(m_port);
	fl->addRow(tr("Порт"), portEdit);

	QLineEdit* userEdit = new QLineEdit(&settingsDialog);
	userEdit->setText(m_user);
	fl->addRow(tr("Пользователь"), userEdit);

	QLineEdit* passwordEdit = new QLineEdit(&settingsDialog);
	passwordEdit->setEchoMode(QLineEdit::Password);
	passwordEdit->setText(m_password);
	fl->addRow(tr("Пароль"), passwordEdit);

	QLineEdit* databaseEdit = new QLineEdit(&settingsDialog);
	databaseEdit->setText(m_database);
	fl->addRow(tr("База"), databaseEdit);

	QDialogButtonBox* buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

	connect(buttonBox, &QDialogButtonBox::accepted, &settingsDialog, &QDialog::accept);
	connect(buttonBox, &QDialogButtonBox::rejected, &settingsDialog, &QDialog::reject);

	fl->addRow(buttonBox);

	settingsDialog.setLayout(fl);
	settingsDialog.setWindowTitle("Изменить настройки");

	if (settingsDialog.exec() != QDialog::Accepted)
	{
		return;
	}

	if (hostEdit->text() != m_host ||
			portEdit->text() != m_port ||
			userEdit->text() != m_user ||
			passwordEdit->text() != m_password ||
			databaseEdit->text() != m_database)
	{
		m_host = hostEdit->text();
		m_port = portEdit->text();
		m_user = userEdit->text();
		m_password = passwordEdit->text();
		m_database = databaseEdit->text();

		QSettings settings;
		settings.setValue("host", m_host);
		settings.setValue("port", m_port);
		settings.setValue("user", m_user);
		settings.setValue("password", m_password);
		settings.setValue("database", m_database);

		reconnect();
	}
}

void MainWindow::onDiagramPressed()
{
	QModelIndexList projectSelection = m_projectView->selectionModel()->selectedRows(0);
	if (projectSelection.count() != 1) {
		QMessageBox::warning(this, tr("Warning"), tr("Должен быть выбран один проект!"));
		return;
	}
	int projectRow = projectSelection[0].row();
	int projectId = m_projectTable->data(m_projectTable->index(projectRow, 0)).toInt();

	if (m_diagram == nullptr) {
		m_diagram = new GanttDiagram();
	}
	m_diagram->calculate(projectId);
	m_diagram->showMaximized();
}

void MainWindow::addProject()
{
	bool ok;
	QString text = QInputDialog::getText(this, "Добавить проект",
										 "Наименование проекта", QLineEdit::Normal,
										 "", &ok);

	if (!ok || text.isEmpty())
	{
		return;
	}

	m_db.transaction();

	QSqlQuery query;
	query.prepare("INSERT INTO projects (project_name) VALUES('" + text + "')");
	if (!query.exec()) {
		QMessageBox::warning(this, "Error", "Невозможно добавить проект: " + query.lastError().text());
		m_db.rollback();
		return;
	}

	m_db.commit();

	updateTable(m_projectTable);
	updateTable(m_jobTable);
	updateTable(m_riskTable);
	onProjectSelectionChanged();
}

void MainWindow::deleteProject()
{
	QModelIndexList selection = m_projectView->selectionModel()->selectedRows(0);
	if (selection.count() == 0)
	{
		QMessageBox::warning(this, tr("Warning"), tr("Ни один проект не выбран!"));
		return;
	}
	for (int i = 0; i < selection.count(); i++)
	{
		m_projectTable->removeRow(selection[i].row());
	}
	updateTable(m_projectTable);
	updateTable(m_jobTable);
	updateTable(m_riskTable);
}

void MainWindow::addJob()
{
	QModelIndexList projectSelection = m_projectView->selectionModel()->selectedRows(0);
	if (projectSelection.count() != 1) {
		QMessageBox::warning(this, tr("Warning"), tr("Должен быть выбран один проект!"));
		return;
	}
	int projectRow = projectSelection[0].row();
	int projectId = m_projectTable->data(m_projectTable->index(projectRow, 0)).toInt();
	bool ok;
	QString text = QInputDialog::getText(this, "Добавить работу",
										 "Наименование работы", QLineEdit::Normal,
										 "", &ok);

	if (!ok || text.isEmpty())
	{
		return;
	}

	QSqlQuery query;
	query.prepare("INSERT INTO works (work_name, project_id) VALUES('" + text + "', " + QString::number(projectId) + ")");
	if (!query.exec()) {
		QMessageBox::warning(this, "Error", "Невозможно добавить работу: " + query.lastError().text());
		return;
	}

	updateTable(m_jobTable);
	updateTable(m_riskTable);
	onJobSelectionChanged();
}

void MainWindow::deleteJob()
{
	QModelIndexList selection = m_jobView->selectionModel()->selectedRows(0);
	if (selection.count() == 0)
	{
		QMessageBox::warning(this, tr("Warning"), tr("Ни одна работа не выбрана!"));
		return;
	}
	for (int i = 0; i < selection.count(); i++)
	{
		m_jobTable->removeRow(selection[i].row());
	}
	updateTable(m_jobTable);
	updateTable(m_riskTable);
}

void MainWindow::addRisk()
{
	QModelIndexList jobSelection = m_jobView->selectionModel()->selectedRows(0);
	if (jobSelection.count() != 1) {
		QMessageBox::warning(this, tr("Warning"), tr("Должен быть выбрана одна работа!"));
		return;
	}
	int jobRow = jobSelection[0].row();
	int jobId = m_jobTable->data(m_jobTable->index(jobRow, 0)).toInt();
	bool ok;
	QString text = QInputDialog::getText(this, "Добавить оценку риска",
										 "Наименование риска", QLineEdit::Normal,
										 "", &ok);

	if (!ok || text.isEmpty())
	{
		return;
	}

	QSqlQuery query;
	query.prepare("INSERT INTO risks (risk_name, work_id) VALUES('" + text + "', " + QString::number(jobId) + ")");
	if (!query.exec()) {
		QMessageBox::warning(this, "Error", "Невозможно добавить оценку риска: " + query.lastError().text());
		return;
	}

	updateTable(m_riskTable);
}

void MainWindow::setRiskImpact()
{
	QModelIndexList selection = m_riskView->selectionModel()->selectedRows(0);
	if (selection.count() != 1) {
		QMessageBox::warning(this, tr("Warning"), tr("Должен быть выбран один риск!"));
		return;
	}
	int riskRow = selection[0].row();
	int riskId = m_riskTable->data(m_riskTable->index(riskRow, 0)).toInt();

	ImpactEditor dialog(riskId);
	dialog.exec();

	updateTable(m_riskTable);
}

void MainWindow::deleteRisk()
{
	QModelIndexList selection = m_riskView->selectionModel()->selectedRows(0);
	if (selection.count() == 0)
	{
		QMessageBox::warning(this, tr("Warning"), tr("Ни одна оценка риска не выбрана!"));
		return;
	}
	for (int i = 0; i < selection.count(); i++)
	{
		m_riskTable->removeRow(selection[i].row());
	}
	updateTable(m_riskTable);
}

void MainWindow::onProjectSelectionChanged()
{
	QModelIndexList projectSelection = m_projectView->selectionModel()->selectedRows(0);
	if (projectSelection.count() != 1) {
		m_jobTable->setFilter("project_id = -1");	// Убираем все элементы, потому что не понятно чьи элементы показывать
		m_riskTable->setFilter("work_id = -1");
		return;
	}
	int projectRow = projectSelection[0].row();
	int projectId = m_projectTable->data(m_projectTable->index(projectRow, 0)).toInt();
	m_jobTable->setFilter("project_id = " + QString::number(projectId));

	onJobSelectionChanged();
}

void MainWindow::onJobSelectionChanged()
{
	QModelIndexList jobSelection = m_jobView->selectionModel()->selectedRows(0);
	if (jobSelection.count() != 1) {
		m_riskTable->setFilter("work_id = -1");	// Убираем все элементы, потому что не понятно чьи элементы показывать
		return;
	}
	int jobRow = jobSelection[0].row();
	int jobId = m_jobTable->data(m_jobTable->index(jobRow, 0)).toInt();
	m_riskTable->setFilter("work_id = " + QString::number(jobId));
}

// Процедура переподключения базы
bool MainWindow::reconnect()
{
	if (m_db.isOpen()) {
		m_db.close();
	}

	m_db.setHostName(m_host);
	m_db.setDatabaseName(m_database);
	m_db.setUserName(m_user);
	m_db.setPassword(m_password);

	if (!m_db.open()) {
		QMessageBox::warning(this, "Connect error", "Could not connect to database, check settings");
		return false;
	}

	return true;
}

void MainWindow::updateTable(QSqlTableModel* table)
{
	if (!table->select()) {
		QMessageBox::warning(this, "Database error", "Could not update project list: " + table->lastError().text());
	}
}
