#include "ganttdiagram.h"
#include <QChart>
#include <QChartView>
#include <QVBoxLayout>
#include <QBarSet>
#include <QHorizontalStackedBarSeries>
#include <QBarCategoryAxis>
#include <QDateTimeAxis>
#include <QDateTime>
#include <QTextEdit>
#include <QSqlQuery>
#include <QSqlError>
#include <cmath>
#include <QMessageBox>

const qint64 MILISECONDS_PER_DAY = 24 * 60 * 60 * 1000;

using namespace QtCharts;

GanttDiagram::GanttDiagram(QWidget *parent) : QWidget(parent)
{
	setWindowTitle("Индикация рисков на диаграмме Ганта");

	QChart* chart  = new QChart;
	QChartView* chartView = new QChartView(chart);
	QVBoxLayout* layout = new QVBoxLayout();
	layout->addWidget(chartView);
    m_log = new QTextEdit(this);
    layout->addWidget(m_log);
	setLayout(layout);

	m_empty = new QBarSet("");	//Сдвиг начала работы
	m_veryLow = new QBarSet("Очень низкий");	//Длительность (в днях) для каждого уровня риска
	m_low = new QBarSet("Низкий");
	m_middle = new QBarSet("Средний");
	m_high = new QBarSet("Высокий");
	m_veryHigh = new QBarSet("Очень высокий");

	m_levels << m_veryLow << m_low << m_middle << m_high << m_veryHigh;

	m_empty->setColor(QColor(255, 255, 255, 0));
	m_veryLow->setColor(Qt::darkGreen);
	m_low->setColor(Qt::green);
	m_middle->setColor(Qt::yellow);
    m_high->setColor(QColor(255, 127, 0));
	m_veryHigh->setColor(Qt::red);

	QHorizontalStackedBarSeries *series = new QHorizontalStackedBarSeries();
	series->append(m_empty);
	for (auto* level : m_levels) {
		series->append(level);
	}

	chart->addSeries(series);
	chart->setTitle("Уровень риска");
	chart->setAnimationOptions(QChart::SeriesAnimations);

	m_axisY = new QBarCategoryAxis();	// Ось Y с описанием работ
	m_axisY->setTitleText("Работы");
	chart->addAxis(m_axisY, Qt::AlignLeft);
	series->attachAxis(m_axisY);

	m_axisX = new QDateTimeAxis;	// Ось X с описанием дат
	m_axisX->setFormat("dd.MM.yyyy");
	m_axisX->setTitleText("Дата");
	m_axisX->setLabelsAngle(90);
	chart->addAxis(m_axisX, Qt::AlignBottom);
	series->attachAxis(m_axisX);
}

void GanttDiagram::calculate(int projectId)
{
	m_log->clear();
	QSqlQuery projectQuery("SELECT project_name, start_date, end_date FROM projects WHERE id = " + QString::number(projectId) + ";");
	reportErrors(projectQuery);
	if (!projectQuery.next()) {
		m_log->append("Невозможно прочитать проект: " + projectQuery.lastError().text());
		return;
	}
	QString projectName = projectQuery.value(0).toString();
	QDateTime projectStartDate = projectQuery.value(1).toDateTime();
	QDateTime projectEndDate = projectQuery.value(2).toDateTime();
	qint64 projectStart = projectStartDate.toMSecsSinceEpoch();
	qint64 projectEnd = projectEndDate.toMSecsSinceEpoch() + MILISECONDS_PER_DAY;
	projectEndDate = projectEndDate.fromMSecsSinceEpoch(projectEnd);
	projectQuery.clear();

	m_axisX->setTickCount((projectEnd - projectStart) / MILISECONDS_PER_DAY + 1);	// Количество дней + 1, потому что вконце ставится отметка даты, в которой работы не ведутся (завершающая метка)
	m_axisX->setRange(projectStartDate, projectEndDate);	// Отображаемый диапазон
	m_log->append("Проект: \"" + projectName + "\"");
	QStringList categories;
	m_axisY->clear();
	clearBars(m_empty);
	for (auto* level : m_levels) {
		clearBars(level);
	}

	QSqlQuery jobQuery("SELECT id, work_name, start_date, end_date FROM works WHERE project_id = " + QString::number(projectId) + " ORDER BY start_date ASC, end_date ASC;");
	reportErrors(jobQuery);
	while (jobQuery.next()) {
		int jobId = jobQuery.value(0).toInt();
		QString jobName = jobQuery.value(1).toString();
		QDateTime jobStartDate = jobQuery.value(2).toDateTime();
		QDateTime jobEndDate = jobQuery.value(3).toDateTime();
		qint64 jobStart = jobStartDate.toMSecsSinceEpoch();
		qint64 jobEnd = jobEndDate.toMSecsSinceEpoch() + MILISECONDS_PER_DAY;
		categories.insert(0, jobName);
        m_log->append("\n\tРабота: \"" + jobName + "\" (" + jobStartDate.toString("dd.MM.yyyy") + ", " + jobEndDate.toString("dd.MM.yyyy") + ")");
		m_empty->insert(0, jobStart);
		for (auto* level : m_levels) {
			level->insert(0, 0);
		}
		QSqlQuery riskQuery("SELECT id, "
							"risk_name, "
							"probability, "
							"impact_budget, impact_budget_weight, "
							"impact_end_date, impact_end_date_weight, "
							"impact_quality, impact_quality_weight, "
							"impact_content, impact_content_weight, "
							"risk_weight "
							"FROM risks "
							"WHERE work_id = " + QString::number(jobId) + ";");
		reportErrors(riskQuery);
		int riskSum = 0;
		QStringList riskSumStrings;
		while (riskQuery.next()) {
			int riskId = riskQuery.value(0).toInt();
			QString riskName = riskQuery.value(1).toString();

			double probability = riskQuery.value(2).toFloat();

			int impactOnBudget = riskQuery.value(3).toInt();
			double weightOfImpactOnBudget = riskQuery.value(4).toFloat();

			int impactOnTime = riskQuery.value(5).toInt();
			double weightOfImpactOnTime = riskQuery.value(6).toFloat();

			int impactOnQuality = riskQuery.value(7).toInt();
			double weightOfImpactOnQuality = riskQuery.value(8).toFloat();

			int impactOnEffect = riskQuery.value(9).toInt();
			double weightOfImpactOnEffect = riskQuery.value(10).toFloat();

			int riskWeight = riskQuery.value(11).toInt();

			m_log->append("\t\tРиск: \"" + riskName + "\"");

			double preciseImpact = (impactOnBudget * weightOfImpactOnBudget +
									impactOnTime * weightOfImpactOnTime +
									impactOnQuality * weightOfImpactOnQuality +
									impactOnEffect * weightOfImpactOnEffect) / 4;

            int impact;

            if (preciseImpact <= 0.25) {
                impact = 1;
            } else if (preciseImpact <= 0.5) {
                impact = 2;
            } else if (preciseImpact <= 0.75) {
                impact = 3;
            } else if (preciseImpact <= 1) {
                impact = 4;
            } else {
                impact = 5;
            }

            //int impact = std::round(preciseImpact);

			m_log->append("\t\t\tВлияние = (" + QString::number(impactOnBudget) + " * " + QString::number(weightOfImpactOnBudget) + " + " +
						  QString::number(impactOnTime) + " * " + QString::number(weightOfImpactOnTime) + " + " +
						  QString::number(impactOnQuality) + " * " + QString::number(weightOfImpactOnQuality) + " + " +
						  QString::number(impactOnEffect) + " * " + QString::number(weightOfImpactOnEffect) + ") / 4 = " +
						  QString::number(preciseImpact) + " ≈ " +
                          QString::number(impact));

			int importanceOfRisk = impact * probability;
			m_log->append("\t\t\tВажность = " +QString::number(impact) + " * " + QString::number(probability));

			QSqlQuery riskUpdateQuery("UPDATE risks "
									  "SET risk_importance = " + QString::number(importanceOfRisk) + " " +
									  "WHERE id = " + QString::number(riskId) + ";");

			riskSum += importanceOfRisk * riskWeight;
			riskSumStrings.append(QString::number(importanceOfRisk) + " * " + QString::number(riskWeight));
		}
		int risk = std::round(riskSum / 100.0);

		QString decision;
        if (risk <= 5) {
			m_veryLow->replace(0, jobEnd - jobStart);
			decision = "очень низкий";
        } else if (risk <= 10) {
			m_low->replace(0, jobEnd - jobStart);
			decision = "низкий";
        } else if (risk <= 15) {
			m_middle->replace(0, jobEnd - jobStart);
			decision = "средний";
        } else if (risk <= 20) {
			m_high->replace(0, jobEnd - jobStart);
			decision = "высокий";
		} else {
			m_veryHigh->replace(0, jobEnd - jobStart);
			decision = "очень высокий";
		}
		m_log->append("\t\t(" + riskSumStrings.join(" + ") + ") / 100 = " +
					  QString::number(riskSum / 100.0) + " ≈ " +
					  QString::number(risk) + " → " +
					  decision + " уровень риска");
	}

	m_axisY->append(categories);
}

void GanttDiagram::reportErrors(const QSqlQuery& query)
{
	if (query.lastError().type() != QSqlError::NoError) {
		QMessageBox::warning(this, "Error", "Невозможно выполнить запрос: " + query.lastError().text());
	}
}

void GanttDiagram::clearBars(QBarSet* barSet)
{
	if (barSet->count() > 0) {
		barSet->remove(0, barSet->count());
	}
}
