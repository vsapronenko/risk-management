USE RiskManagement;

CREATE TABLE Subdivision
(
    idSubdivision INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    subdivisionName VARCHAR(45) NOT NULL UNIQUE
);

CREATE TABLE Employee
(
    idEmployee INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    lastName VARCHAR(45) NOT NULL,
    firstName VARCHAR(45) NOT NULL,
    patronymic VARCHAR(45) NOT NULL,
    employeeSubdivision INT NOT NULL,
    FOREIGN KEY (employeeSubdivision) REFERENCES Subdivision (idSubdivision),
    position VARCHAR(45) NOT NULL,
    headLogin VARCHAR(45),
    headPassword VARCHAR(45)
);

CREATE TABLE Projects (
    idProject INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL, 
    projectName VARCHAR(45) NOT NULL,
    projectDescription VARCHAR(45),
    # projectPerimeter INTEGER DEFAULT NULL,
    # FOREIGN KEY (projectPerimeter) REFERENCES Subdivision (idSubdivision),
    # projectHead INTEGER DEFAULT NULL,
    # FOREIGN KEY (projectHead) REFERENCES Employee (idEmployee),
    budget INTEGER DEFAULT 0, 
    startDate DATE,
    endDate DATE,
    expired INTEGER DEFAULT 0,
    duration INTEGER
);

CREATE TABLE EmployeeToProjects
(
    idEmployee INTEGER NOT NULL,
    FOREIGN KEY (idEmployee) REFERENCES Employee (idEmployee),
    idProject INTEGER NOT NULL,
    FOREIGN KEY (idProject) REFERENCES Projects (idProject)
);

CREATE TABLE Equipment
(
    idEquipment INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    equipmentName VARCHAR(45) NOT NULL,
    usageDuration INTEGER NOT NULL,
    usagePrice INTEGER NOT NULL
);
 
CREATE TABLE Materials
(
    idMaterial INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    materialName VARCHAR(45) NOT NULL,
    unit VARCHAR(45) NOT NULL,
    count INTEGER NOT NULL,
    unitPrice INTEGER NOT NULL
);

CREATE TABLE Works (
    idWork INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL, 
    workName VARCHAR(45) NOT NULL,
    idProject INTEGER NOT NULL,
    FOREIGN KEY (idProject) REFERENCES Projects (idProject) ON DELETE CASCADE,
    startDate DATE,
    endDate DATE,
    expired INTEGER,
    duration INTEGER,
    cost INTEGER,
    # executor INTEGER NOT NULL,
    # FOREIGN KEY (executor) REFERENCES Employee (idEmployee),
    cumulativeJobRisksImortance INTEGER
);


CREATE TABLE EquipmentToWork
(
    idEquipment INTEGER NOT NULL,
    FOREIGN KEY (idEquipment) REFERENCES Equipment (idEquipment),
    idWork INTEGER NOT NULL,
    FOREIGN KEY (idWork) REFERENCES Works (idWork)
);

CREATE TABLE MaterialsToWork
(
    idMaterial INTEGER NOT NULL,
    FOREIGN KEY (idMaterial) REFERENCES Materials (idMaterial),
    idWork INTEGER NOT NULL,
    FOREIGN KEY (idWork) REFERENCES Works (idWork)
);

CREATE TABLE Risks (
    idRisk INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL, 
    riskName VARCHAR(45) NOT NULL,
    idWork INTEGER NOT NULL,
    FOREIGN KEY (idWork) REFERENCES Works (idWork) ON DELETE CASCADE,
    measuresToDecrease VARCHAR(45),
    probability INTEGER,
    impactBudget INTEGER,
    impactBudgetWeight FLOAT,
    impactEndDate INTEGER,
    impactEndDateWeight FLOAT,
    impactQuality INTEGER,
    impactQualityWeight FLOAT,
    impactContent INTEGER,
    impactContentWeight FLOAT,
    riskImportance INTEGER,
    riskWeight INTEGER
);
