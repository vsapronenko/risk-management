﻿CREATE TABLE subdivision
(
    id SERIAL NOT NULL,
    subdivision_name VARCHAR(45) NOT NULL UNIQUE,
    PRIMARY KEY (id)
);

CREATE TABLE employee
(
    id SERIAL NOT NULL,
    last_name VARCHAR(45) NOT NULL,
    first_name VARCHAR(45) NOT NULL,
    patronymic VARCHAR(45) NOT NULL,
    subdivision_id INT NOT NULL,
    FOREIGN KEY (subdivision_id) REFERENCES subdivision (id),
    position VARCHAR(45) NOT NULL,
    head_login VARCHAR(45),
    head_password VARCHAR(45),
    PRIMARY KEY(id)
);

CREATE TABLE projects (
    id SERIAL NOT NULL, 
    project_name VARCHAR(45) NOT NULL,
    project_description VARCHAR(45),
    budget INTEGER DEFAULT 0, 
    start_date DATE,
    end_date DATE,
    expired INTEGER DEFAULT 0,
    duration INTEGER,
    PRIMARY KEY(id)
);

CREATE TABLE employee_to_projects
(
    employee_id INTEGER NOT NULL,
    FOREIGN KEY (employee_id) REFERENCES employee (id),
    project_id INTEGER NOT NULL,
    FOREIGN KEY (project_id) REFERENCES projects (id)
);

CREATE TABLE equipment
(
    id SERIAL NOT NULL,
    equipment_name VARCHAR(45) NOT NULL,
    usage_duration INTEGER NOT NULL,
    usage_price INTEGER NOT NULL,
    PRIMARY KEY(id)
);
 
CREATE TABLE materials
(
    id SERIAL NOT NULL,
    material_name VARCHAR(45) NOT NULL,
    unit VARCHAR(45) NOT NULL,
    count INTEGER NOT NULL,
    unit_price INTEGER NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE works (
    id SERIAL NOT NULL, 
    work_name VARCHAR(45) NOT NULL,
    project_id INTEGER NOT NULL,
    FOREIGN KEY (project_id) REFERENCES projects (id) ON DELETE CASCADE,
    start_date DATE,
    end_date DATE,
    expired INTEGER,
    duration INTEGER,
    cost INTEGER,
    cumulative_job_risks_imortance INTEGER,
    PRIMARY KEY(id)
);


CREATE TABLE equipment_to_work
(
    equipment_id INTEGER NOT NULL,
    FOREIGN KEY (equipment_id) REFERENCES equipment (id),
    work_id INTEGER NOT NULL,
    FOREIGN KEY (work_id) REFERENCES works (id)
);

CREATE TABLE materials_to_work
(
    material_id INTEGER NOT NULL,
    FOREIGN KEY (material_id) REFERENCES materials (id),
    work_id INTEGER NOT NULL,
    FOREIGN KEY (work_id) REFERENCES works (id)
);

CREATE TABLE risks (
    id SERIAL NOT NULL, 
    risk_name VARCHAR(45) NOT NULL,
    work_id INTEGER NOT NULL,
    FOREIGN KEY (work_id) REFERENCES works (id) ON DELETE CASCADE,
    measures_to_decrease VARCHAR(45),
    probability INTEGER,
    impact_budget INTEGER,
    impact_budget_weight FLOAT,
    impact_end_date INTEGER,
    impact_end_date_weight FLOAT,
    impact_quality INTEGER,
    impact_quality_weight FLOAT,
    impact_content INTEGER,
    impact_content_weight FLOAT,
    risk_importance INTEGER,
    risk_weight INTEGER,
    PRIMARY KEY(id)
);
