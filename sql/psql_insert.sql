﻿INSERT INTO public.subdivision (id, subdivision_name) VALUES (1, 'Отдел 1');
INSERT INTO public.subdivision (id, subdivision_name) VALUES (2, 'Отдел 2');
INSERT INTO public.subdivision (id, subdivision_name) VALUES (3, 'Отдел 3');

select setval('subdivision_id_seq', (select max(id) from subdivision));





INSERT INTO public.employee (id, last_name, first_name, patronymic, subdivision_id, position, head_login, head_password) 
	VALUES (1, 'Иванов', 'Иван', 'Иванович', 1, 'Начальник отдела', 'login1', 'password1');
INSERT INTO public.employee (id, last_name, first_name, patronymic, subdivision_id, position, head_login, head_password) 
	VALUES (2, 'Сергиенко', 'Сергей', 'Сергеевич', 1, 'Заместитель начальника отдела', 'login12', 'password2');
INSERT INTO public.employee (id, last_name, first_name, patronymic, subdivision_id, position, head_login, head_password) 
	VALUES (3, 'Сидоров', 'Сидор', 'Сидорович', 1, 'Сотрудник', 'login3', 'password3');
INSERT INTO public.employee (id, last_name, first_name, patronymic, subdivision_id, position, head_login, head_password) 
	VALUES (4, 'Овальная', 'Ирина', 'Петровна', 1, 'Сотрудник', 'login4', 'password4');
	
INSERT INTO public.employee (id, last_name, first_name, patronymic, subdivision_id, position, head_login, head_password) 
	VALUES (5, 'Петров', 'Пётр', 'Петрович', 2, 'Начальник отдела', 'login5', 'password5');
INSERT INTO public.employee (id, last_name, first_name, patronymic, subdivision_id, position, head_login, head_password) 
	VALUES (6, 'Никитин', 'Никита', 'Никитович', 2, 'Заместитель начальника отдела', 'login6', 'password6');
INSERT INTO public.employee (id, last_name, first_name, patronymic, subdivision_id, position, head_login, head_password) 
	VALUES (7, 'Загорная', 'Валентина', 'Ивановна', 2, 'Сотрудник', 'login7', 'password7');
INSERT INTO public.employee (id, last_name, first_name, patronymic, subdivision_id, position, head_login, head_password) 
	VALUES (8, 'Садовый', 'Евгений', 'Павлович', 2, 'Сотрудник', 'login8', 'password8');
	
INSERT INTO public.employee (id, last_name, first_name, patronymic, subdivision_id, position, head_login, head_password) 
	VALUES (9, 'Андреев', 'Андрей', 'Андреевич', 3, 'Начальник отдела', 'login9', 'password9');
INSERT INTO public.employee (id, last_name, first_name, patronymic, subdivision_id, position, head_login, head_password) 
	VALUES (10, 'Павлов', 'Павел', 'Павлович', 3, 'Заместитель начальника отдела', 'login10', 'password10');
INSERT INTO public.employee (id, last_name, first_name, patronymic, subdivision_id, position, head_login, head_password) 
	VALUES (11, 'Андреев', 'Эдуард', 'Андреевич', 3, 'Сотрудник', 'login11', 'password11');
INSERT INTO public.employee (id, last_name, first_name, patronymic, subdivision_id, position, head_login, head_password) 
	VALUES (12, 'Варанина', 'Елизавета', 'Адамовна', 3, 'Сотрудник', 'login12', 'password12');
	
select setval('employee_id_seq', (select max(id) from employee));





INSERT INTO public.projects (id, project_name, project_description, budget, start_date, end_date, expired, duration)
    VALUES (1, 'Проект 1', 'Описание проекта', 100, '2020-02-01', '2020-03-31', 0, 59);
INSERT INTO public.projects (id, project_name, project_description, budget, start_date, end_date, expired, duration)
    VALUES (2, 'Проект 2', 'Описание проекта', 200, '2020-04-01', '2020-06-30', 0, 61);
INSERT INTO public.projects (id, project_name, project_description, budget, start_date, end_date, expired, duration)
    VALUES (3, 'Проект 3', 'Описание проекта', 500, '2020-07-01', '2020-09-30', 0, 92);
    
select setval('projects_id_seq', (select max(id) from projects));



INSERT INTO employee_to_projects (employee_id, project_id) VALUES (1, 1);
INSERT INTO employee_to_projects (employee_id, project_id) VALUES (2, 1);
INSERT INTO employee_to_projects (employee_id, project_id) VALUES (3, 1);
INSERT INTO employee_to_projects (employee_id, project_id) VALUES (4, 1);

INSERT INTO employee_to_projects (employee_id, project_id) VALUES (5, 2);
INSERT INTO employee_to_projects (employee_id, project_id) VALUES (6, 2);
INSERT INTO employee_to_projects (employee_id, project_id) VALUES (7, 2);
INSERT INTO employee_to_projects (employee_id, project_id) VALUES (8, 2);

INSERT INTO employee_to_projects (employee_id, project_id) VALUES (9, 3);
INSERT INTO employee_to_projects (employee_id, project_id) VALUES (10, 3);
INSERT INTO employee_to_projects (employee_id, project_id) VALUES (11, 3);
INSERT INTO employee_to_projects (employee_id, project_id) VALUES (12, 3);





INSERT INTO public.equipment (id, equipment_name, usage_duration, usage_price) VALUES (1, 'Бульдозер', 5, 50);
INSERT INTO public.equipment (id, equipment_name, usage_duration, usage_price) VALUES (2, 'Подъёмник', 8, 45);
INSERT INTO public.equipment (id, equipment_name, usage_duration, usage_price) VALUES (3, 'Грузовик', 3, 30);

select setval('equipment_id_seq', (select max(id) from equipment));





INSERT INTO public.materials (id, material_name, unit, count, unit_price) VALUES (1, 'Цемент', 'кг', 100, 5);
INSERT INTO public.materials (id, material_name, unit, count, unit_price) VALUES (2, 'Кабель', 'м', 80, 10);
INSERT INTO public.materials (id, material_name, unit, count, unit_price) VALUES (3, 'Светильник', 'шт.', 15, 30);





INSERT INTO public.works (id, work_name, project_id, start_date, end_date, expired, duration, cost, cumulative_job_risks_imortance) 
	VALUES (1, 'Заливка фундамента', 1, '2020-02-01', '2020-02-08', 0, 8, 30, NULL);
INSERT INTO public.works (id, work_name, project_id, start_date, end_date, expired, duration, cost, cumulative_job_risks_imortance) 
	VALUES (2, 'Установка новой проводки', 1, '2020-02-09', '2020-03-19', 0, 39, 140, NULL);
INSERT INTO public.works (id, work_name, project_id, start_date, end_date, expired, duration, cost, cumulative_job_risks_imortance) 
	VALUES (3, 'Переоснащение светильников', 1, '2020-03-20', '2020-03-31', 0, 12, 600, NULL);

INSERT INTO public.works (id, work_name, project_id, start_date, end_date, expired, duration, cost, cumulative_job_risks_imortance) 
	VALUES (4, 'Заливка фундамента', 2, '2020-04-01', '2020-04-12', 0, 13, 30, NULL);
INSERT INTO public.works (id, work_name, project_id, start_date, end_date, expired, duration, cost, cumulative_job_risks_imortance) 
	VALUES (5, 'Установка новой проводки', 2, '2020-04-13', '2020-05-13', 0, 29, 140, NULL);
INSERT INTO public.works (id, work_name, project_id, start_date, end_date, expired, duration, cost, cumulative_job_risks_imortance) 
	VALUES (6, 'Переоснащение светильников', 2, '2020-05-13', '2020-05-31', 0, 19, 600, NULL);

INSERT INTO public.works (id, work_name, project_id, start_date, end_date, expired, duration, cost, cumulative_job_risks_imortance) 
	VALUES (7, 'Заливка фундамента', 3, '2020-06-01', '2020-06-30', 0, 30, 30, NULL);
INSERT INTO public.works (id, work_name, project_id, start_date, end_date, expired, duration, cost, cumulative_job_risks_imortance) 
	VALUES (8, 'Заливка фундамента', 3, '2020-07-01', '2020-07-31', 0, 31, 30, NULL);	
INSERT INTO public.works (id, work_name, project_id, start_date, end_date, expired, duration, cost, cumulative_job_risks_imortance) 
	VALUES (9, 'Установка новой проводки', 3, '2020-08-01', '2020-08-31', 0, 31, 140, NULL);
INSERT INTO public.works (id, work_name, project_id, start_date, end_date, expired, duration, cost, cumulative_job_risks_imortance) 
	VALUES (10, 'Переоснащение светильников', 3, '2020-09-01', '2020-09-30', 0, 30, 600, NULL);
	
select setval('works_id_seq', (select max(id) from works));





INSERT INTO equipment_to_work (equipment_id, work_id) VALUES (1, 1);
INSERT INTO equipment_to_work (equipment_id, work_id) VALUES (1, 2);
INSERT INTO equipment_to_work (equipment_id, work_id) VALUES (1, 3);

INSERT INTO equipment_to_work (equipment_id, work_id) VALUES (2, 1);
INSERT INTO equipment_to_work (equipment_id, work_id) VALUES (2, 2);
INSERT INTO equipment_to_work (equipment_id, work_id) VALUES (2, 3);

INSERT INTO equipment_to_work (equipment_id, work_id) VALUES (3, 1);
INSERT INTO equipment_to_work (equipment_id, work_id) VALUES (3, 2);
INSERT INTO equipment_to_work (equipment_id, work_id) VALUES (3, 3);





INSERT INTO materials_to_work (material_id, work_id) VALUES (1, 1);
INSERT INTO materials_to_work (material_id, work_id) VALUES (1, 2);
INSERT INTO materials_to_work (material_id, work_id) VALUES (1, 3);

INSERT INTO materials_to_work (material_id, work_id) VALUES (2, 1);
INSERT INTO materials_to_work (material_id, work_id) VALUES (2, 2);
INSERT INTO materials_to_work (material_id, work_id) VALUES (2, 3);

INSERT INTO materials_to_work (material_id, work_id) VALUES (3, 1);
INSERT INTO materials_to_work (material_id, work_id) VALUES (3, 2);
INSERT INTO materials_to_work (material_id, work_id) VALUES (3, 3);





INSERT INTO public.risks (id, risk_name, work_id, measures_to_decrease, probability, impact_budget, impact_budget_weight, impact_end_date, impact_end_date_weight, 
	impact_quality, impact_quality_weight, impact_content, impact_content_weight, risk_importance, risk_weight) 
    VALUES (1, 'Поломка бульдозера', 1, 'Меры по уменьшению риска', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.risks (id, risk_name, work_id, measures_to_decrease, probability, impact_budget, impact_budget_weight, impact_end_date, impact_end_date_weight, 
	impact_quality, impact_quality_weight, impact_content, impact_content_weight, risk_importance, risk_weight) 
    VALUES (2, 'Короткое замыкание в оборудовании', 1, 'Меры по уменьшению риска', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.risks (id, risk_name, work_id, measures_to_decrease, probability, impact_budget, impact_budget_weight, impact_end_date, impact_end_date_weight, 
	impact_quality, impact_quality_weight, impact_content, impact_content_weight, risk_importance, risk_weight) 
    VALUES (3, 'Неисправность светильников', 1, 'Меры по уменьшению риска', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO public.risks (id, risk_name, work_id, measures_to_decrease, probability, impact_budget, impact_budget_weight, impact_end_date, impact_end_date_weight, 
	impact_quality, impact_quality_weight, impact_content, impact_content_weight, risk_importance, risk_weight) 
    VALUES (4, 'Поломка бульдозера', 2, 'Меры по уменьшению риска', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
    
INSERT INTO public.risks (id, risk_name, work_id, measures_to_decrease, probability, impact_budget, impact_budget_weight, impact_end_date, impact_end_date_weight, 
	impact_quality, impact_quality_weight, impact_content, impact_content_weight, risk_importance, risk_weight) 
    VALUES (5, 'Короткое замыкание в оборудовании', 3, 'Меры по уменьшению риска', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.risks (id, risk_name, work_id, measures_to_decrease, probability, impact_budget, impact_budget_weight, impact_end_date, impact_end_date_weight, 
	impact_quality, impact_quality_weight, impact_content, impact_content_weight, risk_importance, risk_weight) 
    VALUES (6, 'Неисправность светильников', 3, 'Меры по уменьшению риска', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO public.risks (id, risk_name, work_id, measures_to_decrease, probability, impact_budget, impact_budget_weight, impact_end_date, impact_end_date_weight, 
	impact_quality, impact_quality_weight, impact_content, impact_content_weight, risk_importance, risk_weight) 
    VALUES (7, 'Поломка бульдозера', 4, 'Меры по уменьшению риска', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.risks (id, risk_name, work_id, measures_to_decrease, probability, impact_budget, impact_budget_weight, impact_end_date, impact_end_date_weight, 
	impact_quality, impact_quality_weight, impact_content, impact_content_weight, risk_importance, risk_weight) 
    VALUES (8, 'Короткое замыкание в оборудовании', 4, 'Меры по уменьшению риска', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
    
INSERT INTO public.risks (id, risk_name, work_id, measures_to_decrease, probability, impact_budget, impact_budget_weight, impact_end_date, impact_end_date_weight, 
	impact_quality, impact_quality_weight, impact_content, impact_content_weight, risk_importance, risk_weight) 
    VALUES (9, 'Неисправность светильников', 5, 'Меры по уменьшению риска', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
    
select setval('risks_id_seq', (select max(id) from risks));
