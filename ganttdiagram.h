#ifndef GANNTDIAGRAM_H
#define GANNTDIAGRAM_H
#include <QWidget>

namespace QtCharts {
	class QBarSet;
	class QDateTimeAxis;
	class QBarCategoryAxis;
}

class QTextEdit;
class QSqlQuery;

class GanttDiagram : public QWidget
{
	Q_OBJECT
public:
	explicit GanttDiagram(QWidget *parent = nullptr);

	void calculate(int projectId);
	void reportErrors(const QSqlQuery& query);

signals:

private:
	void clearBars(QtCharts::QBarSet* barSet);

	QtCharts::QBarSet *m_empty = nullptr;	//Сдвиг начала работы
	QtCharts::QBarSet *m_veryLow = nullptr;	//Длительность (в днях) для каждого уровня риска
	QtCharts::QBarSet *m_low = nullptr;
	QtCharts::QBarSet *m_middle = nullptr;
	QtCharts::QBarSet *m_high = nullptr;
	QtCharts::QBarSet *m_veryHigh = nullptr;

	QList<QtCharts::QBarSet*> m_levels;

	QtCharts::QDateTimeAxis *m_axisX = nullptr;
	QtCharts::QBarCategoryAxis* m_axisY = nullptr;

	QTextEdit* m_log = nullptr;
};

#endif // GANNTDIAGRAM_H
